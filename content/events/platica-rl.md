---
name: "Detección de Fake News mediante Reinforcement Learning"
date: 2022-10-28
happening: 2022-10-28
status: finish
---

## Objetivo

Revisar el estado del arte del proyecto "Aprendiendo a navegar resultados de
google"

## Ponente

[Alexis Aguilera]({{< ref "/members/alexis-aguilera.md" >}})

## Video

* [Sesión](https://www.youtube.com/watch?v=DE4Hq_yo824)


