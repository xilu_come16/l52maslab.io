---
name: "Taller Transformers"
date: 2022-10-10
happening: 2022-10-10
status: finish
---

## Objetivo

Revisar los conceptos principales de las redes transfomers.

## Ponente

[Ximena Contreras]({{< ref "/members/ximena-contreras.md" >}})

## Videos

* [Sesión 1: Encoder](https://www.youtube.com/watch?v=LVOcqcUsP9g)
* [Sesión 2: Decoder](https://www.youtube.com/watch?v=qxkmHTByNJ0)
* [Sesión 3: Codigo](https://www.youtube.com/watch?v=TbZ3S26hyWM)

### Links extras

* [Las matemáticas de transformers](https://www.youtube.com/watch?v=w5pfPvGGSIY)

