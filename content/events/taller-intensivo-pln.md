---
name: "Taller intensivo PLN"
date: 2020-11-24
happening: 2020-11-24
status: finish
---

## Objetivo

Revisar los conceptos de programación de sistemas basados en Procesamiento de Lenguaje Natural

Carpeta de trabajo [📂](https://drive.google.com/drive/folders/1pPe7TT-e7Yyq3swYwM5IWPo97kO8L_VB?usp=sharing)

## Temario
| Día  hora                        | Tema                                                       | Material                                                                                                                                                                                                                                                                                                                             | Tipo            |
| -------------------------------- | ---------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------- |
| Martes, 11hrs<br>Martes, 12hrs   | Motivación, tareas y APIs de PLN<br>Modelos transformers   | [📺](https://youtu.be/dQY4h3fbjIY) [📈](https://docs.google.com/presentation/d/1TBPepeswL_Q80a3qBCQz8DSgtRTCEgh2yCha1AjroP0/edit?usp=sharing) [📓](https://colab.research.google.com/drive/1xH7dW7rodaYJ0OKUB-3W8AlOQ5DqyTiC?usp=sharing)                                                                                            | Teoría/Practica |
| Viernes, 15hrs<br>Viernes, 16hrs | Etiquetación/Doccano<br>Código para clasificar y etiquetar | [📺](https://youtu.be/T6vgE8aiTHA) [📈](https://docs.google.com/presentation/d/1_UsotNDGMlRuGGSTQVCi12Bph8ZE0SPgn-jEc3GGujU/edit?usp=sharing) [📓](https://colab.research.google.com/drive/1P8Y8z4XJ3zs5HNb9joQJd_z-AaJgddBm?usp=sharing)[📓](https://colab.research.google.com/drive/1LLKJlgX-QVRPuuYBSikfyLyfAColTk_g?usp=sharing) | Teoría/Practica |


## Horario

Martes 24 de noviembre, 11 de la mañana
Viernes 27 de noviembre, 15 de la tarde


## Motivación, tareas y APIs de PLN
1. ¿Por qué lenguaje y computadoras?
2. Algunas tareas comunes en PLN
3. APIs y librerias de PLN


## Transformers
1. Redes neuronales
2. Lenguaje vectorial
3. Modelos *basado en transformers*


## Etiquetación y *doccano*
1. Datos, datos y datos
2. Clasificación y modelos secuenciales
3. Plataforma doccano


## Codificación
1. Pytorch y huggingface
2. Código para clasificación
3. Código para etiquetación de secuencias

