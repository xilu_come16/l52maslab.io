---
title: Corpus de Náhuatl de la Sierra Norte-Oriental y del Norte de Puebla
date: 2021-09-22
publish: True
tags: [náhuatl, reconcimiento de voz, lenguas originarias ]
author: Abraham-Lazaro
author_link: https://www.linkedin.com/in/abrahamjlazaro/ 
license: ccbysa
katex: True
--- 

[https://www.openslr.org/92/](https://www.openslr.org/92/) 

El corpus cuenta con alrededor de 185 horas de grabaciones en Náhuatl de la misma zona, incluso con algunas traducciones en español. Es importante que sea una buena cantidad de horas de la misma variante del náhuatl ya que entre variantes pueden cambiar tantas palabras, sonidos y letras como entre el español y el portugués. La justa cantidad de horas nos permite entrenar modelos de inteligencia artificial basados en machine o deep learning para la traducción o para reconocimiento de voz en esta variante de la lengua náhuatl. 


## Resumen

Corpus de lengua náhuatl que contiene audios de dos zonas: Puebla (185 horas 21 minutos 37 segundos, 954 audios, todas las transcripciones, 299 traducidos al español, se esperan más traducciones) y Zacatlán‐Ahuacatlán‐Tepetzintla (3 horas 1 minutos 27 segundos, 151 audios, transcripción, ninguna traducción al español). Licencia Creative Common


## Descripción

El corpus contiene un documento PDF, en la primera descarga (Deposit-Nahuatl-for-ASR-Community.pdf), el cual explica de manera general el dataset. 

Los archivos se separan en dos conjuntos, Highland Puebla Náhuatl (954 archivos) y Zacatlán‐Ahuacatlán‐Tepetzintla Náhuatl (151 archivos). 

Para el conjunto Highland Puebla Náhuatl, datos de generados de la municipalidad de Cuetzalan del Progreso: 



* Se cuenta con los 954 audios transcritos a Náhuatl. Archivos en formato trs, los podemos encontrar en la carpeta Transcriber-files-all-954 dentro de la primera descarga.
* 738 transcripciones fueron importadas a ELAN para revisión y transcripción al español. 
* Un primer conjunto de 299 archivos ELAN fueron revisados y traducidos al español (carpeta ELAN-files-Final-proofed-and-most-translated de la primera descarga)
* El segundo conjunto con 439 archivos ELAN serán revisados y traducidos próximamente (carpeta ELAN-files-First-draft-only de la primera descarga) (esperar actualizaciones de la documentación)
* Los 954 archivos de audio tratan sobre una variedad de temas, de los cuales la botánica predomina. Checar tabla en el archivo PDF principal en la primera descarga.
* Las demás descargas de Sound-Files-Puebla-Nahuatl contienen los 954 audios en muy alta calidad (por ello pesan tanto) separados por tema de conversación, revisar Deposit-Nahuatl-for-ASR-Community.pdf para revisar los temas tratados en las entrevistas.

Para el conjunto y Zacatlán‐Ahuacatlán‐Tepetzintla Náhuatl de la municipalidad de Tepetzintla:



* Este conjunto fue desarrollado para un proyecto para comparar la botánica entre hablantes de Mixteco, totonaco y náhuatl. 
* Podemos encontrar los 151 audios en la primera descarga en la carpeta de Field-recordings-Tepetzintla-Tonalixco 
* No tienen traducción al español

Para ambos conjuntos, se encuentran anotaciones en la carpeta Metadata de la primera descarga, la cual contiene información útil sobre cada archivo como duración, personas que hablan, tipo de grabación, tema a tratar, entre otros. También existe un archivo con información sobre los participantes.

En la última descarga, _SpeechTranslation_Nahuatl_manifes_t podemos encontrar algunos de los archivos formateados para ELAN para su traducción. Algunas carpetas tienen información extra solo de la división de trabajo para la revisión y traducción de estos al español.

