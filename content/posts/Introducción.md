﻿---
title: Especialización en Deep Learning por Andrew Ng
date: 2022-11-01
publish: True
tags: [deep-learning, curso]
author: ulrich-villavicencio
license: ccbysa
katex: True
--- 

## ¿De qué trata?

[Esta especialización](https://www.coursera.org/specializations/deep-learning), brindada por [Andrew Ng](https://en.wikipedia.org/wiki/Andrew_Ng), te enseñará los fundamentos, retos y consecuencias del Aprendizaje Profundo. Aquí aprenderás a cómo construir y entrenar arquitecturas como Redes Neuronales Convuncionales, Transformers, LSTMs, entre otras, además de cómo optimizarlas con ciertas estrategias, como por ejemplo, Droupout, Inicialización de Xavier/He, entre ortras.

Esta especialización se imparte en la plataforma de Coursera y consta de 5 cursos: 
*Neural Networks and Deep Learning,   
Improving Deep Neural Networks: Hyperparameter Tuning, Regularization and Optimization,  
Structuring Machine Learning Projects,    
Convolutional Neural Networks,   
Sequence Models*

Eso sí, este curso es de nivel intermedio-avanzado. Recomiendo que primero conozcas conceptos de Álgebra Lineal, Cálculo en varias variables y que sepas, al menos, cómo construir una red neuronal simple desde cero. La buena noticia es que en este blog hay posts que te pueden ayudar:  [Aprendizaje profundo por tu cuenta](http://turing.iimas.unam.mx/~ivanvladimir/posts/guia_deep_learning/) y [Libros sobre Inteligencia Artificial](http://turing.iimas.unam.mx/~ivanvladimir/posts/libros_ia/).

##   Redes Neuronales y Aprendizaje Profundo

Aquí se verán los conceptos de las redes neuronales y el aprendizaje profundo. En este curso todavía no se usarán tecnologías como tensorflow o pytorch, sino que lo harás todo desde 0. Implementarás todos los algoritmos necesarios para crear tu red neuronal con las capas profundas que desees.

## Mejorando Redes Neuronales Profundas: Ajustes de Hiperparámetros, Regularización y Optimización

Verás técnicas para mejorar las redes neuronales que hagas. Particularmente, verás cómo obtener los mejores posibles ajustes para los parámetros e hiperparámetros, qué algoritmos usar para ciertos problemas, qué funciones usar de activación, entre otras opciones de optimización.

## Estructurando proyectos de Machine Learning

Este curso te brindará conceptos para tomar buenas decisiones a la hora de construir y manejar projectos de Machine Learning. Verás cómo identificar errores y usar estrategias para reducirlos, aplicar transfer learning, multi-task learning, end-to-end learning, entre otros. El objetivo de este curso es darte una idea de cómo se empieza un projecto de Machine Learning y cómo se va manteniendo en un ambiente laboral.

##   Redes Neuronales Convolucionales

Aquí verás uno de los conceptos más grandes para la visión por computadora: las redes neuronales convolucional. Verás cómo funcionan y cómo aplicarlas para algunos proyectos muy interesantes.

## Modelos Secuenciales

Finalmente, se te presentará los modelos secuenciales y su aplicación hacía el reconocimiento de voces, chatbots, NLP, validación de identidad, entre otros.

## Links recomendados

- [El curso.](https://www.coursera.org/learn/neural-networks-deep-learning?specialization=deep-learning)
- [Lista de 4 videos por 3Blue1Brown](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi) que te puede ayudar con tu intuición sobre algunos de los conceptos que se presentan en este curso.
- [Neural Networks and Deep Learning](https://static.latexstudio.net/article/2018/0912/neuralnetworksanddeeplearning.pdf), por Michael Nielsen.
- El capítulo de [Logistic Regression](https://web.stanford.edu/~jurafsky/slp3/5.pdf) te ayudará a entender de una manera más matemática la razón de algunas ecuaciones que estarás viendo en este curso. Ampliamente recomendable si deseas profundizar en esta cuestión.
