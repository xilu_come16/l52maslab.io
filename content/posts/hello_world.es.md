---
title: Hola mundo
date: 2021-03-30
publish: True
tags: [python, flask]
author: Ivan-Meza
license: ccbysa
--- 


**Hello World**, desde nuestro nuevo blog!!

``` python
    print "hello world"
```

Este es un ejemplo sencillo.
