﻿---
title: Estructurando proyectos de Machine Learning
date: 2022-11-04
publish: True
tags: [deep-learning, curso]
author: ulrich-villavicencio
license: ccbysa
katex: True
--- 


## ¿Qué harás?

Verás cómo construir de manera exitosa un proyecto en Machine Learning. Este curso está basado ampliamente en la experiencia de Andrew Ng. 

En este curso entenderás cómo identificar errores en un sistema de Machine Learning, priorizar la dirección más prometedora para reducir errores, entender cómo manipular de manera correcta los conjuntos de entrenamiento y testeo, entender cómo usar *end-to-end, transfer, multi-tasking learning.*

## Estrategias de Machine Learning

Supongamos que ya hiciste un modelo y tiene un 90% de precisión, pero quieres mejorarlo aún más. Algunas ideas que puedes tener son, por ejemplo,

 - Recolectar más datos.
 - Recolectar un conjunto de entrenamiento más diverso.
 - Entrenar por más iteraciones el descenso de gradiente.
 - Probar otro algoritmo en vez del descenso de gradiente.
 - Probar con una red más grande.
 - Probar con una red más pequeña.
 - Usar regularizaciones.
 -  ...
 
 Como puedes ver, la lista anterior puede seguir creciendo tanto como queramos. El problema es que si escoges mal una dirección de qué hacer, esto puede llevar a mucho tiempo cambiando ajustes para después ver que no hiciste nada o se mejoro mínimante.

Así, la meta del curso es que veas algunas estrategias que nos pueden mostrar las mejores posibles opciones a probar dependiendo del problema.

### Ortogonalización

Lo definimos como el proceso de saber qué cambiar en orden para lograr un efecto deseado. Un ejemplo de esto es a la hora de usar un telescopio, ya que necesitas calibrar varias piezas y posicionarlo de manera adecuada para poder ver lo que quieres ver; si llegas a calibrar mal una pieza o usar otra pieza que no debías, lo vas a notar de inmediato y, si sabes usar telescopios, sabrás qué hacer.

Para aprendizaje supervisado necesitas modificar parámetros e hiperparámetros para un eficiente modelo. Por ende, en esta parte del curso verás cómo usar este concepto para el conjunto de entrenamiento, dev y de testeo.

## ¿Qué es lo que quieres lograr?

Para dar respuesta a esto, tendrás que tener una métrica que te ayude a evaluar y te dirá si algo que probaste es buena idea o no. Así, en esta sección verás cómo plantear una métrica para el problema que quieres atacar. Además, verás cómo usar múltiples métricas y que sea lo más eficiente usarlas (como por ejemplo, la precisión del conjunto de entrenamiento y el tiempo en que se tarda en ejecución).

## Comparar al rendimiento humano

Actualmente algunos modelos de Machine Learning han tenido mejor rendimiento que los humanos, haciendo que sea muy útil para bastantes aplicaciones. Además, resulta que el proceso de diseñar un modelo de Machine Learning es mucho más eficiente cuando, en cambio, se intenta con humanos. 

En esta parte del curso obtendrás herramientas de cómo podemos aprovecharnos de los humanos. En efecto, si nuestro modelo es peor que lo que haría un humano, entonces podríamos hacer, por decir algunas, obtener datos tales que los haya clasificado un humano, un mejor análisis de varianza/sesgo, entre otras.

Verás algunos retos que conlleva mejorar el modelo cuando este está debajo del rendimiento de humanos. De hecho, se vuelve muy díficil mejorar nuestro modelo una vez que hemos llegado a un rendimiento similar al de los humanos.

Para darnos una idea, ya se ha superado a los humanos en modelos para recomendación de productos, publicidad en línea, etc. También es muy probable que conozcas el caso en el que [AlphaGo venció al campeón de Go.](https://www.deepmind.com/research/highlighted-research/alphago)

## Análisis de errores

Aquí verás en mayor profundidad el cómo mejorar modelos cuando su eficiencia es peor que el de los humanos. Posteriormente verás cómo limpiar los datos que el modelo haya clasificado de manera incorrecta; esto lo harás analizando los porcentajes de error en los distintos conjuntos de datos y dependiendo del caso, tendrás que decidir qué hacer.

Además, en esta sección verás muchos ejemplos de varios modelos con distintos casos y verás que te recomienda hacer Andrew Ng.

## Discordancia del conjunto de datos

Al estar usando muchos datos, los modelos funcionan mejor cuando puedes etiquetar de manera correcta el conjunto de entrenamiento. Aquí verás algunas de las mejores prácticas para saber qué hacer con tu conjunto de entrenamiento y dev/test. Igual que antes, verás varios ejemplos con distintas sitaciones y Andrew Ng te mostrará las cosas que él haría en cada una de ellas.

Algunas técnicas que verás son usar análisis de error, obtener más datos similres a los conjuntos de dev/test, crear más datos usando *Artificial data synthesis* (verás algo de esto pero no en profundidad), entre otras.

### Transfer learning

Consiste en aplicar lo que hiciste en un modelo y usarlo en otro. Por ejemplo, si tienes un clasificador de gatos, puedes usar parte de la red neuronal para clasificar rayos x. Verás cómo hacerlo y algunas consideraciones que se deben de hacer.

### Multi-task learning

Aquí empiezas con dos modelos de manera simultánea. Lo que se espera, y se desea, es que se ayuden entre sí para un mejor resultado. Por ejemplo, si quieres entrenar una red para clasificar peatones y señales de alto, se podrían transferir conocimientos entre si para la identificación de ciertos objetos. Esto resulta mucho mejor que entrenarlas por separado.

Por supuesto, al igual que lo anteior, verás cómo implementarlo y todo lo que tienes que tomar a cuenta para que sea posible hacer esta técnica. También aprenderás sobre la técnica de *end-to-end deep learning*, que es básicamente implementar todas las etapas de una red neuronal en un sola. Igual verás cómo hacerlo y las ventajas y desventajas que conlleva.


