---
title: Deep Learning. Traducción. ( SSH. llaves. Túnel )
date: 2021-11-25
publish: True
tags: [traducción, lenguas originarias, ssh, tunel]
author: Alexander Guido
author_link: "www.linkedin.com/in/alxguido"
license: ccbysa
katex: True
--- 

Este video presenta los mecanísmos para conectarse a un servidor usando 'ssh'
para que en el futuro en ese servidor se ejecuten las notebooks que entrenan un
modelo de traducción.

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/JmoNjPJTijA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>
