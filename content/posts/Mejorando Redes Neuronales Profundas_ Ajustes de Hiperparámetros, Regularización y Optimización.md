﻿---
title: "Mejorando Redes Neuronales Profundas: Ajustes de Hiperparámetros, Regularización y Optimización"
date: 2022-11-03
publish: True
tags: [deep-learning, curso]
author: ulrich-villavicencio
license: ccbysa
katex: True
--- 

## ¿Qué harás?

En este curso entenderás cómo aumentar el rendimiento de tu red neuronal y obtener mejores resultados. Verás lo siguiente:

-  Entender las mejores prácticas para construir redes neuronales.
- Usar y entender algoritmos de optimización, como regularizacón, normalización de batches, RMSprop, Adam, etc.
- Implementar una red neuronal usando TensorFlow.

## Aspectos prácticos

En esta primera sección verás cómo dividir nuestros conjuntos de datos para entrenar de manera efectiva a la red. Esto lo harás por los conjuntos *train, dev* y *test*. Además, verás lo importante que es estar experimentando con nuestra red neuronal para un buen ajuste de parámetros e hiperparámetros.

### Train/dev/test

La meta será que trates de hacer un modelo con el conjunto de entrenamiento y trates de optimizar los hiperparámetros en el conjunto de dev (este conjunto de datos sirve para hacer validaciones e, idealmente, para optimizar la red). Finalmente, lo evaluas con el conjunto de testing.

Una pregunta natural es saber cómo dividir nuestro conjunto de datos en los conjuntos anteriormente dados. Esto lo verás y te darás cuenta que esto depende mucho de la cantidad de datos que tengas y del problema a atacar. Además, verás algunas cosas importantes que tienes que saber sobre estos conjuntos y cómo usarlos de manera correcta.

### Varianza y sesgos

Este concepto es fundamental para saber qué está haciendo tu red neuronal. Aquí verás los casos que se pueden dar, después del entrenamiento. Verás qué tienes que hacer en cada situación y entenderás cómo es que se produce esto.

## Regularizar la red neuronal

La principal razón por la que queremos regularizar es para reducir la varianza (*overfitting*). Verás las normas L1 y L2 y cómo usarlas en las funciones de coste.

Verás cómo es que esto nos ayuda a reducir el *overfitting*.  En resumidas cuentas, todo dependerá del nuevo hiperparámetro que vamos a introducir (es decir, si es muy grande o no).

Posteriormente, verás una técnica muy usada: *dropout regulraization*. Consiste en desactivar neuronas en cada iteración usando probabilidades. Verás cómo implementarla y la idea detrás de esta técnica.

### Otros métodos de regularización

- Aumento de datos
Esto se usa, por ejemplo, en visión de datos al girar todas las imagenes horizontalmente para tener más datos. Igual rotar desde una posición aleatoria es un claro ejemplo.

- Parada temprana

	Básicamente verás que llega un punto en donde ves el coste de tus conjuntos de entrenamiento y de dev. Llegará un punto en donde, a partir de una iteración, el coste de del conjunto DEV ya no dejará de decrecer, sino empezará a incrementar. Por tanto, verás cómo obtener ese punto crítico que nos puede ayudar bastante.

## Algoritmos de optimización

### Gradiente del descenso en MINI-BATCH

¿Qué harías si tienes un conjunto de entrenamiento de, digamos, 50 millones de elementos (vaya, un número muy grande que sea tardado en procesar para tu red neuronal)? Si lo implementas como has estado viendo en el primer curso y parte de este, te tomará bastante tiempo de proceso cada paso. 

Así, verás lo poderoso que es partir tu conjunto de entrenamiento en MINI BATCHES. Harás particiones de tu conjunto de entrenamiento y en cada partición usarás el descenso de gradiente; la gran ventaja es que esto se puede vectorizar.

En esta parte verás por qué, cuándo y cómo usarlo. Además, verás algunos consejos para que hagas este proceso sencillo como, por ejemplo, hacer un número de particiones de potencias de 2, entre otras.

###   Promedios ponderados exponencialmente

Este es un paso intermedio para entender cómo se puede optimizar el descenso de gradiente. Este algoritmo ayuda a describir series de tiempo. Además, lo importante de este algoritmo es que le designa pesos bajos a obeservaciones pasadas y va actualizando pesos a obeservaciones nuevas. Todo esto y más lo verás en esta parte, incluyendo la intuición y las fórmulas necesarias.

### RMSprop

Este algoritmo nos ayudará a hacer que la función de coste, que la usamos para el descenso del gradiente, converga más rápido a un punto. Por ende, el descenso de gradiente será más rápido. Esta técnica fue introducida en la plataforma de este curso, [Coursera](https://www.coursera.org/).

### Adam

Básicamente combina los dos anteriores algoritmos en uno. Este algoritmo es muy usado y funciona bastante bien en muchas arquitecturas de redes neuronales. Tendrás que implementarlo y verás los hiperparámetros que introduce a la red.

### Decaimento de la taza de aprendizaje

Verás que el problema de usar mini-batch es que el descenso de gradiente no converge, pero usando este algoritmo hará que llegue a valores cercanos del punto de convergencia. En este caso observarás que no hay un consenso como tal y que varias personas toman decisiones distintas sobre este algoritmo.

## Modificación de hiperparámetros

La meta es sacar el máximo rendimiento de nuestros hiperparámetros que estemos usando. Ejemplos de estos son, por mencionar algunos, la taza de aprendizaje, el tamaño de los mini-bnatch, número de capas, funciones de activación, entre otros.

Verás que cada problema es un mundo; necesitan distintas modificaciones de hiperparámetros para funcionar de manera correcta. Además, verás los métodos que hay para optimizarlos de manera correcta y cómo automatizarlos.

Finalmente, en esta sección, verás qué puedes hacer en casos cuando tienes mucho poder computacional y casos en los que no.

## Normalización de batches

En esta sección verás, además de implementarlo, cómo este algoritmo, creado por los investigadores Sergey Loffe y Christian Szegdey, ayuda a acelerar el aprendizaje.

La idea es la siguiente: ¿Se puede normalizar el output de una capa para entrenar los sesgos y pesos de la siguiente capa? Verás el algoritmo y lo tendrás que implementar. Posteriormente, verás cómo usarlo en frameworks como por ejemplo, Tensorflow: `tf.nn.batch-normalization()`. 

Además, verás el por qué este algoritmo funciona, algunas consecuencias que conlleva usarlo, en qué momentos usarlo. 

## Clasificación de múltiples clases

Aquí verás *softmax regression*. Anteriormente se usó clasificación binaria, ahora tendrás que generalizarlo para múltiples clases. Verás que resulta sencillo usando matrices y numpy; las ideas de todos los algoritmos anteriormente vistos siguen siendo las mismas.

## Frameworks de aprendizaje profundo

Finalmente, ya que hayas visto cómo funcionan e implementado varios de los algoritmos fundamentales para las redes neuronales, verás cómo usar todos los anteriores algoritmos en Tensorflow. Ahora ya no nos interesa volver a inventar la rueda,  sino aprovechar todo lo de los frameworks para generar nuestras redes neuronales de manera eficiente. [Aquí](https://en.wikipedia.org/wiki/Comparison_of_deep_learning_software) puedes encotrar una comparasión de los distintos frameworks que hay.

Así pues, verás una breve introducción a Tensorflow y tendrás que usar varios algoritmos que ya hiciste anteriormente. Verás la estructura que debe de llevar un programa en este framework y la gran facilidad que proporciona.


