---
name: "Eduardo Alvarez"
img: https://s.gravatar.com/avatar/63fc1bb4f9ebaaf4e1ccd92866d64978?s=80
date: 2021-09-29
role: 
    es: "Servicio Social"
    en: "Social service"
active: true
now: 
---

Egresado de la Facultad de Estudios Superiores Aragón de la UNAM en la carrera
de ingeniería en computación. Actualmente trabajando en un proyecto de extracción de información donde se
trata de recopilar la trayectoria académica de individuos mediante búsquedas por
internet.
