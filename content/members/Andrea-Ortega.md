---
name: "Andrea Ortega"
img: https://s.gravatar.com/avatar/1373b3a90baa8728f60f084790ae8ec8?s=80
date: 2021-10-21
role: 
    es: "Servicio Social"
    en: "Social service"
active: true
twitter: c_aandreitaa
---

Física egresada de la Facultad de Ciencias. Actualmente realizo mi servicio social en proyecto "Identificación de aves".

