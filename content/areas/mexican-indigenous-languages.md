---
title: "Lenguas Indígenas Mexicanas"
date: 2019-02-09T17:41:23-06:00
draft: false
status: active
---

Este proyecto busca crear tecnología de lenguaje para Lenguas Indígenas Mexicanas:

* Traductores
* Analizadores de morfología
* Diccionarios

Para lenguas indígenas, si quieres saber más sobre esta área checa los proyectos disponibles.




