---
title: "Aspecto Metalingúísticos "
date: 2019-02-09T23:12:46-06:00
draft: false
status: active
code: metalinguistic
---

En esta área exploramos aspectos metalingüísticos de los textos, es decir que no son explicitos como el humor y el sentir de los textos. En particular hemos hecho investigación en:

* Detección de ironía
* Generación de memes
* Clasificación de sentimientos  
