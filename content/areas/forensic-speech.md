---
title: "Voz con enfoque forense"
date: 2019-02-09T23:07:49-06:00
draft: false
status: active
code: forensicspeech
---

La voz es medio de transmición de la comunicación humana. Esta al desarrollarse en el aparato fonador del emisor adquiere características sonoras del este o esta. Esto implica que esta lleva una firma de identidad del hablante. En esta área exploramos diferentes técnicas para establecer una relación entre la evidencia acústica embebida en la voz y su fortaleza como prueba forense.


