---
title: "Inteligencia Artificial para Biología"
date: 2019-02-09T23:28:13-06:00
draft: false
status: active
code: biology
---

Muchas de las tecnicas que usamos para procesar el lenguaje, ya sea textual o hablado, pueden ser transferidas a otra área. En particular, en esta línea de investigación hemos identificado los siguientes temas de interés en los campos de biología y medicina:

* Exploración de reconocimiento de patrones en el DNA
* Identificación de cantos de aves
* Caracterización de caminata
