---
title: "Código de conducta"
date: 2021-01-21T15:50:17-06:00
---

El Laboratorio L52+ tiene un fin académico, orientado a la investigación.  La
interacción entre los miembros del laboratorio y las diferentes comunidades académicas se debe realizar bajo el respeto mutuo y con conductas apropiadas que permitan el trabajo de manera amena y en un ambiente de cordialidad mutua.

El Laboratorio L52+ aspira a ser una experiencia libre de hostigamientos de cualquier tipo, sea este por género, racial, por religión, edad, color de piel, apariencia, nacionalidad, orientación sexual, identidad de género o capacidades diferentes.

En busca de alcanzar estos objetivos, no se permitirán conductas tales como:

* Comentarios degradantes y/o ofensivos
* Acoso
* Intimidación
* Contacto físico inapropiado
* Uso inapropiado de información
* Cualquier tipo de acto o comentario que vulnere a personas o grupos

<hr/>

Recomendamos la lectura de los siguientes documentos y su práctica continua

* [Código de ética UNAM](http://www.ifc.unam.mx/pdf/codigo-etica-unam.pdf )
* [Guía de derechos y deberes de los estudiantes de la
    UNAM](https://archivos.juridicas.unam.mx/www/bjv/libros/10/4603/2.pdf)
* [Código de Ética y Conducta Profesional de ACM](https://www.acm.org/about-acm/code-of-ethics-in-spanish)


