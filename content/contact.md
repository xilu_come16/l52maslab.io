---
title: "Contacto"
date: 2019-01-21T15:50:17-06:00
---

Nos puedes enviar un <a href="mailto:ivanvladimir+l52mas@gmail.com">correo
aquí</a> o nos puedes contactar a través de nuestra cuenta de <a
href="https://twitter.com/l52mas">tuiter <ion-icon
name="logo-twitter"></ion-icon></a>.

